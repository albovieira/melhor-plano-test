import React, { Component } from 'react';
import {
  Button,
  Alert,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText
} from 'reactstrap';
import http from './services/http';

import 'bootstrap/dist/css/bootstrap.css';
import './App.css';

class App extends Component {
  state = {
    bundles: []
  };

  async componentWillMount() {
    try {
      const { data } = await http.get('bundles');
      this.setState({
        bundles: data
      });
    } catch (error) {
      console.log(error);
    }
  }

  generateBundle = async () => {
    try {
      this.setState({
        bundles: []
      });
      const { data } = await http.post('bundles');
      this.setState({
        bundles: data.bundles
      });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title"> Bundle Generator </h1>
        </header>
        <div className="container">
          <div className="row" style={{ marginTop: '2rem' }}>
            <Button
              outline
              color="primary"
              size="lg"
              block
              onClick={this.generateBundle}
            >
              Gerar
            </Button>
          </div>
          <div className="row">
            {this.state.bundles.length > 0
              ? this.state.bundles.map(bundle => {
                  return (
                    <Card
                      className="col-4"
                      style={{
                        marginTop: '2rem',
                        padding: '1rem'
                      }}
                    >
                      <CardBody>
                        <CardTitle>
                          <strong> Price: </strong> {bundle.price}
                        </CardTitle>
                        <CardText>
                          {bundle.bundles.map(i => i.name).join(' + ')}
                        </CardText>
                      </CardBody>
                    </Card>
                  );
                })
              : ''}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
