import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import routes from './router';

const server = express();
const PORT = process.env.PORT || 3000;
server.use(express.static('public'));
server.use(express.static('app/build'));

server.use(cors());
server.use(bodyParser.json());
routes(server);
server.listen(PORT, async () => {
  console.log(`Server running on port ${PORT} in ${process.env.NODE_ENV} mode`);
});
