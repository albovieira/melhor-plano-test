import BundlesRoutes from './bundles';

const urlBase = '/api/v1';
const routes = server => {
  server.use(`${urlBase}/bundles`, BundlesRoutes);
  server.use((req, res, next) => {
    res.status(500).json({
      status: 500,
      msg: 'Error on route. This route exist?',
      route: req.originalUrl
    });
    next();
  });
};

export default routes;
