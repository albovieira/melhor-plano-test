import generateBundles from '../../services/generate-bundles';

const createBundles = async (req, res) => {
  try {
    const bundles = await generateBundles();
    return res.status(200).json({ bundles });
  } catch (err) {
    console.log(err);
    return res.status(status).json(err);
  }
};
export default createBundles;
