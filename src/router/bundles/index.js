import express from 'express';
import create from './create';
import list from './list';

const router = express.Router();
router.get('/', list);
router.post('/', create);

export default router;
