const util = require('util');
const fs = require('fs');
const writeFile = util.promisify(fs.writeFile);

export default async function recordBroadbands(bundles) {
  const filteredBundles = bundles.filter(
    bundle =>
      bundle.bundles.findIndex(
        internalBundle => internalBundle.type === 'bb'
      ) != -1
  );

  const sortedBundles = filteredBundles.sort((bundle1, bundle2) => {
    const priceDiference = bundle1.price - bundle2.price;
    if (priceDiference !== 0) {
      return priceDiference;
    }
    return bundle2.bundles.length - bundle1.bundles.length;
  });

  try {
    const allBroadbandString = await writeFile(
      `${__dirname}/../../database/all-broadbands.json`,
      JSON.stringify(sortedBundles, null, 2)
    );
  } catch (err) {
    console.log(err);
  }

  return sortedBundles;
}
