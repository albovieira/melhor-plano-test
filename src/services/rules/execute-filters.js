import * as filter from '../filters';
import { bundleGeneration } from '../../database/bundles.json';

export default async function executeFilters(bundles) {
  const filter_rules = bundleGeneration.filter_rules;

  for (let i = 0; i < filter_rules.length; i++) {
    const filter_rule = filter_rules[i];
    bundles = await filterFunctions(filter_rule.fn)(bundles);
  }

  return bundles;
}

function filterFunctions(fn) {
  const filtersFn = {
    'filter-unique': filter.filterUnique,
    'filter-duplicated-bundles': filter.filterDuplicatedBundles
  };
  return filtersFn[fn];
}
