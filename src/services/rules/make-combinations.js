import { bundleGeneration } from '../../database/bundles.json';

export default async function makeCombinations() {
  const nodes = bundleGeneration.nodes.map(node => ({
    _id: node._id,
    name: node.name,
    type: node.type,
    price: node.price,
    father: node.father,
    dependency: node.dependency
  }));

  const bundles = [];

  const combinations = generateCombinations(nodes, 1);
  for (let combo of combinations) {
    bundles.push({
      price: 0.0,
      bundles: combo
    });
  }

  return bundles;
}

function* generateCombinations(arr, minSize) {
  function* doGenerateCombinations(offset, combo) {
    if (combo.length >= minSize) {
      yield combo;
    }
    for (let i = offset; i < arr.length; i++) {
      const currentNode = arr[i];
      if (currentNode.father) {
        const hasFather =
          combo.map(c => c.type).filter(type => type === currentNode.father)
            .length === 0;

        if (hasFather) {
          continue;
        }
      }
      if (currentNode.dependency) {
        const hasDependency =
          combo.map(c => c._id).filter(id => id === currentNode.dependency)
            .length === 0;

        if (hasDependency) {
          continue;
        }
      }
      yield* doGenerateCombinations(i + 1, combo.concat(currentNode));
    }
  }
  yield* doGenerateCombinations(0, []);
}
