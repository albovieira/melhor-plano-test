export default async function filterUnique(bundles) {
  const filteredBundles = bundles.filter(bundle => {
    const typeCount = {};
    const uniqueTypes = ['bb', 'tv', 'll'];

    for (const intervalBundle of bundle.bundles) {
      typeCount[intervalBundle.type] =
        typeCount[intervalBundle.type] !== undefined
          ? typeCount[intervalBundle.type] + 1
          : 1;
    }

    for (const uniqueType of uniqueTypes) {
      if (typeCount[uniqueType] > 1) {
        return false;
      }
    }

    const countUniqueType = bundle.bundles
      .map(v => v.type)
      .reduce((total, type) => {
        if (uniqueTypes.includes(type)) {
          total++;
        }
        return total;
      }, 0);

    if (countUniqueType == 0) {
      return false;
    }

    return true;
  });

  return filteredBundles;
}
