import allbroadbands from './database/all-broadbands.json';

export default async function listBroadbands() {
  return allbroadbands;
}
