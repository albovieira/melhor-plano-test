import { bundleGeneration } from '../database/bundles.json';
import * as rules from './rules';

export default async function generateBundle() {
  const nodes = bundleGeneration.nodes;
  const generation_rules = bundleGeneration.generation_rules;
  const filter_rules = bundleGeneration.filter_rules;

  let bundles = [];
  for (let i = 0; i < generation_rules.length; i++) {
    const generation_rule = generation_rules[i];
    if (rulesFunctions(generation_rule.fn)) {
      bundles = await rulesFunctions(generation_rule.fn)(bundles);
    }
  }

  return bundles;
}

function rulesFunctions(fn) {
  const rulesFn = {
    'make-combinations': rules.makeCombinations,
    'execute-filter-rules': rules.executeFilter,
    'sum-nodes-prices': rules.sumNodes,
    'sum-connections-values': rules.sumConnectionValues,
    'record-broadbands': rules.recordBroadbands
  };
  return rulesFn[fn] || false;
}
